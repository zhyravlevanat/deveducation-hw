mocha.setup('bdd');

const assert = chai.assert;

// HANDLER FUNCTIONS
describe('onReadDataBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call getData function', function() {
    const stub = sandbox.stub(window, 'getData');
              
	  readDataBtn.click();
  
    sandbox.assert.calledOnce(stub);        
  });   
});   

describe('onSaveDataBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call saveData function', function() {
    const stub = sandbox.stub(window, 'saveData');
            
	  saveDataBtn.click();
  
    sandbox.assert.calledOnce(stub);          
  }); 
}); 
describe('onClearDataBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call clearTableData function', function() {
    const stub = sandbox.stub(window, 'clearTableData');
              
	  clearDataBtn.click();
  
    sandbox.assert.calledOnce(stub);   
  });
});  
describe('onDeleteBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call isIdDefined function', function() {
    const stub = sandbox.stub(window, 'isIdDefined');
        
    userDeleteBtn.click();
  
    sandbox.assert.calledOnce(stub);          
  });  
});

describe('onUserSearchBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call getUserData function', function() {
    const stub = sandbox.stub(window, 'getUserData');
        
	  userSearchBtn.click();
  
    sandbox.assert.calledOnce(stub);        
  });  
});

describe('onUserUpdateBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call isFormValid function', function() {
    const stub = sandbox.stub(window, 'isFormValid');
        
	  userUpdateBtn.click();
  
    sandbox.assert.calledOnce(stub);        
  });  
});

describe('onUserAddBtnClick', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call isFormValid function', function() {
    const stub = sandbox.stub(window, 'isFormValid');
        
	  userAddEndBtn.click();
  
    sandbox.assert.calledOnce(stub);        
  });  
});

//DATA MANIPULATION FUNCTIONS

describe('isIdDefined', function() {
  it('should return false if id isnt used', function() {
    users = [{ID: "5", firstName: "Harry", lastName: "Potter", age: "45"}];
    const id = 6;

    const actual = isIdDefined(id);

    const expected = false;
    assert.deepEqual(actual, expected);
  });

  it('should return true if id was used', function() {
    users = [{ID: "5", firstName: "Harry", lastName: "Potter", age: "45"}];
    const id = 5;

    const actual = isIdDefined(id);

    const expected = true;
    assert.deepEqual(actual, expected);
  });  
});

describe('addUserData', function() {
  it('should add element to array', function() {
    users = [{ID: "4", firstName: "Jonh", lastName: "Doe", age: "45"}];
    const user = {ID: "5", firstName: "Harry", lastName: "Potter", age: "45"};

    addUserData(user);
    const actual = users.length;

    const expected = 2;
    assert.deepEqual(actual, expected);
  })
});

describe('getUserData', function() {
  it('should return element by id', function() {
    users = [{ID: "4", firstName: "Jonh", lastName: "Doe", age: "45"}, 
             {ID: "5", firstName: "Harry", lastName: "Potter", age: "45"}];
    const id = 4;
      
    const actual = getUserData(id);

    const expected = {ID: "4", firstName: "Jonh", lastName: "Doe", age: "45"}
    assert.deepEqual(actual, expected);     
  });  
});

describe('deleteUserData', function() {
  it('should delete element by id', function() {
    users = [{ID: "4", firstName: "Jonh", lastName: "Doe", age: "45"}, 
             {ID: "5", firstName: "Harry", lastName: "Potter", age: "45"}];
    const id = 4;
    
    deleteUserData(id);
    const actual = users.length;

    const expected = 1;
    assert.deepEqual(actual, expected);     
  });  
});

describe('updateUserData', function() {
  it('should update element data', function() {
    users = [{ID: "4", firstName: "Jonh", lastName: "Doe", age: "45"}, 
             {ID: "5", firstName: "Harry", lastName: "Potter", age: "45"}];
    const newElem = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"};

    updateUserData(newElem);  
    const actual = users[1];

    const expected = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"};
    assert.deepEqual(actual, expected);     
  });  
});

//DOM-ELEMENTS MANIPULATION FUNCTIONS

describe('renderUserElement', function() {
  it('should add element to table', function() {
    usersDataTableElement.innerHTML = '';
    const user = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"}

    renderUserElement(user);
    const actual = usersDataTableElement.children.length;

    const expected = 1;
    assert.deepEqual(actual, expected);     
  });  
});

describe('deleteUserElement', function() {
  it('should add element to table', function() {
    usersDataTableElement.innerHTML = '';
    const user = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"};
    renderUserElement(user);
    const id = 5;
    
    deleteUserElement(id);
    const actual = usersDataTableElement.children.length;

    const expected = 0;
    assert.deepEqual(actual, expected);     
  });  
});

describe('clearTableData', function() {
  it('should add element to table', function() {
    const user = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"};
    renderUserElement(user);
        
    clearTableData();
    const actual = usersDataTableElement.children.length;

    const expected = 1;
    assert.deepEqual(actual, expected);     
  });  
});

//FORM MAINPULATION FUNCTION

describe('isFormValid', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call isFielValid function', function() {
    const stub = sandbox.stub(window, 'isFieldValid');
    
    isFormValid();    
  
    sandbox.assert.calledOnce(stub);
           
  });   
}); 

describe('isFieldValid', function() {
  it('should return true if value is number', function() {
    const elem = document.createElement('input');
    elem.value = 5;
        
    const actual = isFieldValid(elem);

    const expected = true;
    assert.deepEqual(actual, expected);     
  });
  
  it('should return true if value is string', function() {
    const elem = document.createElement('input');
    elem.value = 'a';
        
    const actual = isFieldValid(elem);

    const expected = true;
    assert.deepEqual(actual, expected);     
  });  

  it('should return false if value is empty', function() {
    const elem = document.createElement('input');
    elem.value = '';
        
    const actual = isFieldValid(elem);

    const expected = false;
    assert.deepEqual(actual, expected);     
  });  
});

describe('isFieldValidWithReg', function() {
  it('should return false if value doesn`t match reg exp', function() {
    const elem = document.createElement('input');
    elem.value = 5;
        
    const actual = isFieldValidWithReg(elem);

    const expected = false;
    assert.deepEqual(actual, expected);     
  });
  
  it('should return true if value matches reg exp', function() {
    const elem = document.createElement('input');
    elem.value = 'a';
        
    const actual = isFieldValidWithReg(elem);

    const expected = true;
    assert.deepEqual(actual, expected);     
  }); 
  
  it('should return false if value doesn`t match reg exp', function() {
    const elem = document.createElement('input');
    elem.value = '%';
        
    const actual = isFieldValidWithReg(elem);

    const expected = false;
    assert.deepEqual(actual, expected);     
  }); 

  it('should return false if value is empty', function() {
    const elem = document.createElement('input');
    elem.value = '';
        
    const actual = isFieldValidWithReg(elem);

    const expected = false;
    assert.deepEqual(actual, expected);     
  });  
});

describe('setFormValues', function() {
  it('should set values in form inputs from elem', function() {
    const user = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"};
            
    setFormValues(user);
    const actual = userLastNameInput.value;

    const expected = "Potter";
    assert.deepEqual(actual, expected);
    
    resetUserForm();
  });
    
});

describe('getFormValues', function() {
  it('should return object with values from form inputs', function() {
    userIdInput.value = 5;
    userFirstNameInput.value = 'John';
    userLastNameInput.value = 'Doe';
    userAgeInput.value = 45;
            
    const actual = getFormValues();
    
    const expected = {
      ID: '5',
      firstName: 'John',
      lastName: 'Doe',
      age: '45'
    };
    assert.deepEqual(actual, expected);     
  });  
});

describe('resetUserForm', function() {
  it('should make all form fields empty or in initial state', function() {
    userIdInput.value = 5;
    userFirstNameInput.value = 'John';
    userLastNameInput.value = 'Doe';
    userAgeInput.value = 45;
    
    resetUserForm();
    const actual = {
      1: userFirstNameInput.value,
      2: userAgeInput.value,
      3: userIdInput.value,
      4: userLastNameInput.value
    }

    const expected = {
      1: '',
      2: '',
      3: '',
      4: ''
    };
    assert.deepEqual(actual, expected);
    
    users = [];
  });    
});

//LOCAL STORAGE FUNCTIONS

describe('saveData', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call setItem function', function() {
    const stub = sandbox.stub(localStorage, 'setItem');
        
	  saveData();
  
    sandbox.assert.calledOnce(stub);        
  });  
});

describe('getData', function() {
  let sandbox;
  
  before(() => {
    sandbox = sinon.createSandbox();
  });
  
  afterEach(() => {
    sandbox.restore();
  });

  it('should call getItem function', function() {
    const stub = sandbox.stub(localStorage, 'getItem');
        
	  getData();
  
    sandbox.assert.calledOnce(stub);        
  });  
});

describe('createUserElement', function() {
  it('should call fillRect function', function() {
    const user = {ID: "5", firstName: "Harry", lastName: "Potter", age: "35"};

    const actual = createUserElement(user);  

    const row = document.createElement('tr');
    row.setAttribute('data-id', 5);
    row.innerHTML = `<td>5</td>
                     <td>Harry</td>
                     <td>Potter</td>
                     <td>35</td>`;

    const expected = row.toString();
    assert.equal(actual, expected);     
  });  
});
mocha.run();