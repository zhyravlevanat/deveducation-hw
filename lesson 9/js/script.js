const usersDataTableElement = document.getElementById('usersDataTable'),
      tableDataHeaderElement = document.getElementById('tableDataHeader'),
      userDataFormElement = document.getElementById('userDataForm'),
      userIdInput = document.getElementById('userIdInput'),
      userFirstNameInput = document.getElementById('userFirstNameInput'),
      userLastNameInput = document.getElementById('userLastNameInput'),
      userAgeInput = document.getElementById('userAgeInput'),      
      userSearchBtn = document.getElementById('userReadBtn'),
      userUpdateBtn = document.getElementById('userUpdateBtn'),
      userDeleteBtn = document.getElementById('userDeleteBtn'),
      userAddStartBtn = document.getElementById('addStartBtn'),
      userAddMiddleBtn = document.getElementById('addMiddleBtn'),
      userAddEndBtn = document.getElementById('addEndBtn'),
      clearDataBtn = document.getElementById('clearTableData'),
      saveDataBtn = document.getElementById('saveTableData'),
      readDataBtn = document.getElementById('readTableData'),
      addUserBtns = document.getElementById('addBtns')
      userItemTemplate = document.getElementById('userItemTemplate').innerHTML;


let users = [];

// EVENT HANDLERS
userDeleteBtn.addEventListener('click', onDeleteBtnClick);
userSearchBtn.addEventListener('click', onUserSearchBtnClick);
userUpdateBtn.addEventListener('click', onUserUpdateBtnClick);
userAddStartBtn.addEventListener('click', onUserAddBtnClick);
userAddMiddleBtn.addEventListener('click', onUserAddBtnClick);
userAddEndBtn.addEventListener('click', onUserAddBtnClick);
clearDataBtn.addEventListener('click', onClearDataBtnClick);
saveDataBtn.addEventListener('click', onSaveDataBtnClick);
readDataBtn.addEventListener('click', onReadDataBtnClick);
usersDataTableElement.addEventListener('click', onTableElementDataClick);


// HANDLER FUNCTIONS
function onDeleteBtnClick() {
  const id = userIdInput.value;
  if (isIdDefined(id)) {
    deleteUserData(id);
    deleteUserElement(id);
    resetUserForm();
  } 
}

function onUserSearchBtnClick() {
  const id = userIdInput.value;
  if (isIdDefined(id)) {
    const user = getUserData(id);
    setFormValues(user);
  }
}

function onUserUpdateBtnClick() {
  if (isFormValid()) {
    const user = getFormValues();
    const newElem = createUserElement(user);
    const currentElement = getUserElement(user.ID);
    currentElement.replaceWith(newElem);
    updateUserData(user);
    resetUserForm();
  }
}

function onUserAddBtnClick(e) {
  if (isFormValid()) {
    const btn = e.target.dataset.add;
    const user = getFormValues();
    if (!isIdDefined(user.ID)) { 
      const currentElem = createUserElement(user); 
      addUserData(user);    
      switch(btn) {
        case 'start':       
          tableDataHeaderElement.after(currentElem);
          break;
        case 'middle': 
          const indexInMiddle = getMiddleIndex();
          usersDataTableElement.children[indexInMiddle-1].after(currentElem);
          break;
        case 'end':
          usersDataTableElement.append(currentElem); 
          break;
      }
      resetUserForm();
    } else {
      setTimeout(userIdInput.classList.add('invalid'),1000);
    }
  }
}

function onClearDataBtnClick() {
  clearTableData();  
}

function onSaveDataBtnClick() {
  saveData();
}

function onReadDataBtnClick() {
  users = getData();
  if (users) {
    users.forEach(user => renderUserElement(user));
  }
}

function onTableElementDataClick(e) {
  if (e.target.tagName !== 'TD') return;  

  const id = event.target.parentElement.dataset.id;
  const user = getUserData(id);
  setFormValues(user);
}

//DATA MANIPULATION FUNCTIONS

function isIdDefined(id) {
  return getUserData(id) ? true : false
}

function addUserData(user) {
  users.push(user); 
}

function getUserData(id) {
  return users.find(user => user.ID == id);
}

function deleteUserData(id) {
  users = users.filter(user => user.ID != id);
}

function updateUserData(newData) {
  users.forEach(user => {
    if (user.ID == newData.ID) {
      user.age = newData.age;
      user.firstName = newData.firstName;
      user.lastName = newData.lastName;      
    }   
  });  
}

//DOM-ELEMENTS MANIPULATION FUNCTIONS
function renderUserElement(user) {
  const userItem = createUserElement(user);
  usersDataTableElement.append(userItem);
}

function createUserElement(user){
  const userTr = document.createElement('tr');
  userTr.setAttribute('data-id', user.ID);
  
  userTr.innerHTML = userItemTemplate
              .replace('{{ID}}', user.ID)
              .replace('{{FirstName}}', user.firstName)
              .replace('{{LastName}}', user.lastName)
              .replace('{{Age}}', user.age);
 return userTr;
}

function getUserElement(id) {
  return usersDataTableElement.querySelector(`[data-id="${id}"]`);
}

function deleteUserElement(id) {
  const user = getUserElement(id);
  user.remove();
}

function clearTableData() {
  usersDataTableElement.innerHTML = '';
  usersDataTableElement.append(tableDataHeaderElement);
}

//FORM MAINPULATION FUNCTION
function isFormValid(){
  return isFieldValid(userIdInput)
          && isFieldValidWithReg(userFirstNameInput)
          && isFieldValidWithReg(userLastNameInput)
          && isFieldValid(userAgeInput)
}

function isFieldValid(elem){
  if (elem.value){
      elem.classList.remove('invalid');
      return true;
  } else {
      elem.classList.add('invalid');
      return false;
  }
}

function isFieldValidWithReg(elem){
  var regName = /^[a-zA-Z]+$/;
  elem.value = elem.value.trim();
  if (elem.value && regName.test(elem.value)){
      elem.classList.remove('invalid');
      return true;
  } else {
      elem.classList.add('invalid');
      return false;
  }
}
function setFormValues({ID, firstName, lastName, age}) {
    userIdInput.value = ID;
    userFirstNameInput.value = firstName;
    userLastNameInput.value = lastName;
    userAgeInput.value = age;  
}

function getFormValues() {
  return {
    ID: userIdInput.value,
    firstName: userFirstNameInput.value,
    lastName: userLastNameInput.value,
    age: userAgeInput.value
  }
}

function resetUserForm(){
  userDataFormElement.reset();  
}

//LOCAL STORAGE FUNCTIONS
function saveData() {
  localStorage.setItem('users', JSON.stringify(users));
}

function getData() {
  const users = localStorage.getItem('users');

  return users ? JSON.parse(users) : [];
}

// ADDITIONAL FUNCTIONS 
function getMiddleIndex() {
  return Math.ceil((users.length)/2);
}


      
