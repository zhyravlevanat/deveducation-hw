class DoubleLList {
  root = null;
  length = 0;

  createNode(value) {
      return {
          value,
          next: null,
          prev: null,
      };
  }

  push(...rest) {
      let lastNode = this.root;
      
      if (lastNode) {
          while(lastNode.next) {
              lastNode = lastNode.next;
          }
      }

      for (const value of rest) {
          const node = this.createNode(value);

          if (!lastNode) {                   
            lastNode = this.root = node;
            continue;
          }
          lastNode.next = node;
          node.prev = lastNode;
          lastNode = lastNode.next;
      }

      this.length += rest.length;

      return this.length;
  }

  pop() {
      let deletedElement = void 0;
      let node = this.root;

      if (!node) {
          return deletedElement;
      } else if (!node.next) {
          deletedElement = node.value;
          this.root = null;
          return deletedElement;
      } 

      while (node.next) {
          node = node.next;
      }

      deletedElement = node.value;
      node.prev.next = null;
      node = null;
     
      this.length = this.length - 1;

      return deletedElement;
  }

  unshift(...rest) {
      if (!rest.length) {
          return this.length;
      }

      let newFirstNode = null;
      let firstNode = null;

      for (const value of rest) {
          const node = this.createNode(value);

          if (!firstNode) {
              firstNode = node;
              newFirstNode = node;
            } else {
              firstNode.next = node;
              node.prev = firstNode;
              firstNode = firstNode.next;
          }
      }
      if (this.root) {
        firstNode.next = this.root;
        this.root.prev = firstNode;
      } 
      
      this.root = newFirstNode;
      this.length += rest.length;

      return this.length;
  }

  shift() {
      let deletedElement = void 0;

      if (!this.root) {
          return deletedElement;
      } else if (!this.root.next) {
        deletedElement = this.root.value;
        this.root = null;
      } else {
        deletedElement = this.root.value;
        this.root = this.root.next;
        this.root.prev = null; 
         
      }

      this.length = this.length - 1;

      return deletedElement;
  }

  toString() {
      let printNode = this.root;
      let result = '';

      if (!printNode) {
          return result;
      }

      while(printNode) {
          result += `${printNode.value},`;
          printNode = printNode.next;
      }

      return result;
  }
}

//const lList = new DoubleLList();

// console.log(lList.unshift(1));
// console.log(lList.unshift(2, 5));
// console.log(lList.shift());
// console.log(lList.toString());


