describe('push', () => {
  
  it('should insert node at the end of the list, if root is null, elem will be assigned to the root, return increased length of list ', () => {

    const list = new DoubleLList();
    list.root = null;
    const elem = 3;

    const actual = list.push(elem);

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('should insert node at the end of the list return increased length of list ', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};
    list.length = 1;
    const elem = 2;

    const actual = list.push(elem);
   
    const expected = 2;
    assert.equal(actual, expected);
  });

  it('should insert 2 nodes at the end of the list return increased length of list', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};
    list.length = 1;
    const elem1 = 2;
    const elem2 = 3;
 
    const actual = list.push(elem1, elem2);
    
    const expected = 3;
    assert.equal(actual, expected);
  });
});

describe('pop', () => {
  
  it('should delete 1 element from the end of list return value of deleted element', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};

    const actual = list.pop();

    const expected = 3;
    assert.deepEqual(actual, expected);
  });

  it('if no elements in list return undefined ', () => {

    const list = new DoubleLList();
    list.root = null;

    const actual = list.pop();

    const expected = undefined;
    assert.equal(actual, expected);
  });  
});

describe('shift', () => {
  
  it('should delete 1 element from the beginning of list return value of deleted element', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};

    const actual = list.shift();

    const expected = 3;
    assert.deepEqual(actual, expected);
  });

  it('if no elements in list return undefined ', () => {

    const list = new DoubleLList();
    list.root = null;

    const actual = list.shift();

    const expected = undefined;
    assert.equal(actual, expected);
  });  
});

describe('unshift', () => {
  
  it('should insert node at the beginning of the list, if root is null, elem will be assigned to the root, return increased length of list  ', () => {

    const list = new DoubleLList();
    list.root = null;
    const elem = 3;

    const actual = list.unshift(elem);

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('should insert node at the beginning of the list, return increased length of list  ', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};
    list.length = 1;
    const elem = 3;

    const actual = list.unshift(elem);

    const expected = 2;
    assert.equal(actual, expected);
  }); 

  it('should insert 2 nodes at the beginning of the list, return increased length of list  ', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};
    list.length = 1;
    const elem1 = 3;
    const elem2 = 5;

    const actual = list.unshift(elem1, elem2);

    const expected = 3;
    assert.equal(actual, expected);
  }); 
});

describe('toString', () => {
  
  it('should return string with all values of list separated by commas', () => {

    const list = new DoubleLList();
    list.root = {value: 3, next: null, prev: null};

    const actual = list.toString();

    const expected = '3,';
    assert.equal(actual, expected);
  });

  it('if there are no elements in list should return empty string ', () => {

    const list = new DoubleLList();
    list.root = null;

    const actual = list.toString();

    const expected = '';
    assert.equal(actual, expected);
  });  
});

