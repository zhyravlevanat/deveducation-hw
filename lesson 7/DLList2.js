class DoubleLListWithHeadAndTail {
  head = null;
  tail = null;

  length = 0;

  createNode(value) {
      return {
          value,
          next: null,
          prev: null,
      };
  }

  push(...rest) {
      let lastNode = this.head;
      
      if (lastNode) {
          while(lastNode.next) {
              lastNode = lastNode.next;
              
          }
      }
      this.tail = lastNode;
      for (const value of rest) {
          const node = this.createNode(value);

          if (!this.head) {                   
            this.head = node;
            this.tail = node;
          } else {
            node.prev = this.tail;
            this.tail.next = node;
            this.tail = node;
          }
      }

      this.length += rest.length;

      return this.length;
  }

  pop() {
      let deletedElement = void 0;
      let node = this.head;

      if (!node) {
          return deletedElement;
      } else if (!node.next) {
          deletedElement = node.value;
          this.root = null;
          return deletedElement;
      } 

      while (node.next) {
          node = node.next;
          this.tail = node;
      }

      deletedElement = this.tail.value;
      this.tail = this.tail.prev;
      this.tail.next = null;
      this.length = this.length - 1;

      return deletedElement;
  }

  unshift(...rest) {
      if (!rest.length) {
          return this.length;
      }

      let newFirstNode = null;
      let firstNode = null;

      for (const value of rest) {
          const node = this.createNode(value);

          if (!firstNode) {
              firstNode = node;
              newFirstNode = node;
            } else {
              firstNode.next = node;
              node.prev = firstNode;
              firstNode = firstNode.next;
          }
      }
      if (!this.head) {
        this.tail = firstNode;
      } else {
        firstNode.next = this.head;
        this.head.prev = firstNode;
      }
       
      
      this.head = newFirstNode;
      this.length += rest.length;

      return this.length;
  }

  shift() {
      let deletedElement = void 0;

      if (!this.head) {
          return deletedElement;
      } else if (!this.head.next) {
        deletedElement = this.head.value;
        this.head = null;
        this.tail = null;
      } else {
        deletedElement = this.head.value;
        this.head = this.head.next; 
        this.head.prev = null;
      }

      this.length = this.length - 1;

      return deletedElement;
  }

  toString() {
      let printNode = this.head;
      let result = '';

      if (!printNode) {
          return result;
      }

      while(printNode) {
          result += `${printNode.value},`;
          printNode = printNode.next;
      }

      return result;
  }
}

//const lList = new DoubleLList();

// console.log(lList.unshift(1));
// console.log(lList.unshift(2, 5));
// console.log(lList.shift());
// console.log(lList.toString());


