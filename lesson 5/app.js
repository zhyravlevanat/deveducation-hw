const container = document.getElementById('container');
const URL = 'https://jsonplaceholder.typicode.com/users';
container.addEventListener('click', onListElementClick);

getData(URL);

function getData(url) {
  return fetch(url)
          .then(res => res.json())
          .then(setData)
          .then(data => renderList(container, data))
}

function setData(data) {
  if (data instanceof Array) {
    const obj = {}
    data.forEach((element, index) => {
      obj[`User ${index+1}`] = element;
    }); 
    return obj
  } else if (data instanceof Object) {
    return data;
  } else {
    return
  }
}

function renderList(container, list) {
  container.append(renderListElements(list));
}

function renderListElements(obj) {
  if (typeof(obj) != 'object') return;

  let listElement = document.createElement('ul');
  listElement.className = 'list row';

  for (let key in obj) {
    let listItemElement = document.createElement('li');
    listItemElement.className = 'list-item';

    if (typeof (obj[key]) == 'object') {
      listItemElement.innerHTML = `${key} `;
    } else {
      listItemElement.innerHTML = `${key} : ${obj[key]}`;
    }    

    let subListElement = renderListElements(obj[key]);
    
    if (subListElement) {
      subListElement.className = 'sub-list';
      subListElement.hidden = true;
      listItemElement.append(subListElement);      
    }

    listElement.append(listItemElement);    
  }
  return listElement;
}

function onListElementClick(event) {
  if (event.target.tagName != 'LI') {
    return;
  } 
  let dataContainer = event.target.firstElementChild;
  if (!dataContainer) return;
  
  dataContainer.hidden = !dataContainer.hidden;
}