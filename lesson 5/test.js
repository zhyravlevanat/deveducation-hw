describe('setData', () => {
  
  it('should return object with values as elems of array ', () => {

    const array = [
      {
        "id": 0,
        "name": "England Mcintosh"
      },
      {
        "id": 1,
        "name": "Cline Landry"
      },
      {
        "id": 2,
        "name": "Vang Hopkins"
      }
    ];

    const actual = setData(array);
    
    const expected = {
      'User 1' : {
        "id": 0,
        "name": "England Mcintosh"
      },
      'User 2' : {
        "id": 1,
        "name": "Cline Landry"
      },
      'User 3' : {
        "id": 2,
        "name": "Vang Hopkins"
      }
    };
    assert.deepEqual(actual, expected);
  });

  it('if arg is an object should return object', () => {

    const obj = {
      "id": 0,
      "name": {
        "firstName" : "England",
        "lastName" : "Mcintosh"
      }
    }

    const actual = setData(obj);
    
    const expected = {
      "id": 0,
      "name": {
        "firstName" : "England",
        "lastName" : "Mcintosh"
      }
    };
    assert.deepEqual(actual, expected);
  });

  it('if arg is a primitive should return object', () => {

    const elem = 'string'

    const actual = setData(elem);
    
    const expected = undefined;
    assert.deepEqual(actual, expected);
  });
});

describe('renderListElements', () => {
  
  it('should return list with sublists ', () => {

    const obj = {
      'User 1' : {
        "id": 0,
        "name": "England Mcintosh"
      }      
    };

    const actual = renderListElements(obj);
    
    let listElem = document.createElement('ul');
    listElem.className = 'list row';
    let sublistElem = document.createElement('ul');
    sublistElem.classList = 'sub-list';
    sublistElem.setAttribute('hidden', '');
    let sublistItem1 = document.createElement('li');
    sublistItem1.className = 'list-item';
    sublistItem1.innerHTML = `id : 0`;
    let sublistItem2 = document.createElement('li');
    sublistItem2.className = 'list-item';
    sublistItem2.innerHTML = `name: England Mcintosh`;
    sublistElem.append(sublistItem1, sublistItem2);
    let listItem = document.createElement('li');
    listItem.classList = 'list-item';
    listItem.innerHTML = `User 1 `;
    listItem.innerHTML += sublistElem.outerHTML;
    listElem.append(listItem);
    const expected = listElem;    
    assert.ownInclude(actual, expected);
  });  
});