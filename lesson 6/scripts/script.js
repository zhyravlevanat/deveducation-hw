'use strict';

const calcBtnsContainer = document.getElementById('calcBtnsContainer');
const calcDisplay = document.getElementById('display');
const calcAnsDisplay = document.getElementById('displayAns');
const invertedBtns = document.querySelectorAll('.calculator__button_inverted');
const originFuncs = {
  'sin': 'sin', 
  'ln': 'ln', 
  'cos': 'cos', 
  'log' : 'log', 
  'tan' : 'tan', 
  '√' : '√', 
  'Ans' : 'Ans',
  '**': 'x<sup>y</sup>'
};

const invertedFuncs =  {
  'acrsin' : 'sin<sup>-1</sup>',
  'e**' : 'e<sup>x</sup>',
  'arccos': 'cos<sup>-1</sup>',
  '10**' : '10<sup>x</sup>', 
  'arctan' : 'tan<sup>-1</sup>', 
  '**2': 'x<sup>2</sup>',
  'Rnd': 'Rnd',
  '√√':'<sup>y</sup>√x'
};

let inputs = ["","",""];
let additionInputs = [];
let values = [];
let percentage = false;
let deg = false;
let inverted = false;

init();

function init() {
  calcDisplay.value = '0';
  calcAnsDisplay.value = `Ans = 0`;
}

calcBtnsContainer.addEventListener('click', onCalcBntContainerClick);

function onCalcBntContainerClick(e) {
  if (e.target.innerHTML == 'Inv') {
    inverted = !inverted 
    if (inverted) {
      changeButtonsValues(invertedFuncs);
    } else {
      changeButtonsValues(originFuncs);
    }
  } else if (e.target.innerHTML == 'Deg' || e.target.innerHTML == 'Rad') {
    makeButtonActive(e.target);
  } else if (e.target.innerHTML == 'Ans') {
    setAnsValue();
  } else if (e.target.innerHTML == 'Rnd') {
    setRndValue();
    changeButtonsValues(originFuncs);
  } else {
    calcCompute(e.target.dataset.id);   
  }
}

function changeButtonsValues(obj) {
  let i = 0;
  for (let key in obj) {
    invertedBtns[i].value = key;
    invertedBtns[i].innerHTML = obj[key];
    i++;
  }
  return invertedBtns
}

function makeButtonActive(elem) {
  if (elem.dataset.id == '1' && !deg) return;
  if (elem.dataset.id == '1' && deg) {
    elem.classList.remove('calculator__button_non-active');
    getElementById(2).classList.add('calculator__button_non-active');
    deg = false;
  } else if (elem.dataset.id == '2') {
    deg = true;
    elem.classList.remove('calculator__button_non-active');
    getElementById(1).classList.add('calculator__button_non-active');
  }  
}

function setAnsValue() {
  const ansValue = calcAnsDisplay.value.slice(6);
  calcDisplay.value = ansValue
  values.push(ansValue);
  return calcDisplay.value
}

function setRndValue() {
  calcDisplay.value = Math.random();
  values.push(calcDisplay.value);
  return calcDisplay.value
}

function getElementById(id) {
  return document.querySelector(`[data-id = "${id}"]`);
}

function calcCompute(id)  {
  const elem = getElementById(id);
  const value = elem.value;
  let result;
  switch(true) {
    case (id >= 11 && id < 14) || (id >= 18 && id < 21) || 
         (id >= 26 && id < 29) || (id == 33) || (id == 34):
      if (additionInputs[0] == '(') {
        additionInputs.push(value);
        displayAddData();
      } else {
        values.push(value);
        result = values;
        displayData();
      }
    break;
    case (id == 3):
        values.push(value);
        displayData();
        updateData(values.join(""));
        clearValues();
        result = inputs;
        calcFactorial(inputs);  
    break;
    case (id == 6):
        updateData(values.join(""));
        values.push(value);
        displayData();
        clearValues();
        result = inputs;
        calcPercentage(inputs);  
    break;
    case (id >= 4 && id < 6):
      if (id == 5) {
        additionInputs.push(value);
        displayAddData();
        calcAdditional();
        additionInputs = [];           
      } else {
        additionInputs.push(value);
        displayAddData();
      }      
    break;
    case (id == 15): 
      values.push(Math.PI);
      result = values;
      displayData();
    break;
    case (id == 22): 
      values.push(Math.E);
      result = values;
      displayData();
    break;
    case (id == 9 ||  id == 10 || id == 14 || id == 16 || id == 17 ||  id == 21 ||
          id == 23 || id == 24 ||id == 29 || id == 31 || id == 32 || id == 36):
      if (additionInputs[0] == '(') {
        additionInputs.push(value);
        displayAddData();
      } else {
      updateData(values.join(""));
      updateData(value);
      result = inputs;
      clearValues();
      displayData();
    }
    break;
    case (id == 35):
      updateData(values.join(""));
      clearValues();
      result = inputs;
      calc(inputs);      
    break;
    case (id == 7):
      result = deleteData(values);
      displayData();
    break;    
  }
  
  return result;
}

const addNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum + secondNum;
  }
}
const multNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum * secondNum;
  }
};
const subsNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum - secondNum;
  }
};
const divNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum / secondNum;
  }
};

const convertNumbWithE = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum*Math.pow(10, secondNum);
  }
};

const changeDegToRad = degree => {
  return degree*Math.PI / 180;
}

function calcAdditional() {
  const str = additionInputs.join('').slice(1, additionInputs.length-1);
  const val = eval(str);
  values.push(val);
}

function calcPercentage(inputs) {
  let res;
  switch (inputs[1]) {
    case "+": 
      res = divNumbers(multNumbers(Number(inputs[0]), Number(inputs[2])), 100);
      calcDisplay.value = res;
      inputs.pop();
      inputs.unshift('');
      values.push(res);       
    break;
    case "-": 
      res = divNumbers(multNumbers(Number(inputs[0]), Number(inputs[2])), 100);
      inputs.pop();
      inputs.unshift('');
      values.push(res);       
    break;
    case "×": 
      res = divNumbers(multNumbers(Number(inputs[0]), Number(inputs[2])), 100);
      clearData();
      values.push(res);
      displayData();
      updateAnsValue(values[0]);
    break;
  }
  return values;
}

function calcFactorial(inputs) {
  const val = inputs[2].slice(0, -1);
  clearData();
  values.push(getFactorial(val));
  calcAnsDisplay.value = `Ans = ${values[0]}`;
  displayData();  
}

function getFactorial(number) {
  number = Number(number);
  if (number < 0 || !isFinite(number) || isNaN(number) || (number ^ 0) !== number) return 'not valid data'; 
  if (number < 0) return NaN;   
  let result = 1;
  for (let i = 1; i <= number; i++) 
    result = result * i;
  return result;
}

const calc = (inputs) => {
  let rad;
  switch (inputs[1]) {
    case "+": 
      const sum = addNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();	
      values.push(sum);
    break;
    case "-": 
      const diff = subsNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();	
      values.push(diff);
    break;
    case "×": 
      const mult = multNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();	
      values.push(mult);
    break;
    case "÷": 
      const div = divNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();
      values.push(div);
    break;
    case "**": 
      const pow = Math.pow(Number(inputs[0]), Number(inputs[2]));
      clearData();
      values.push(pow);
    break;
    case "E": 
      const EXP = convertNumbWithE(Number(inputs[0]), Number(inputs[2]));
      clearData();
      values.push(EXP);
    break;
    case "cos": 
      if (deg) {
        rad = changeDegToRad(Number(inputs[2]))
      } else {
        rad = Number(inputs[2]);
      }
      const cos = Math.cos(rad);
      clearData();	
      values.push(cos);
    break;
    case "arccos":
      if (deg) {
          rad = changeDegToRad(Number(inputs[2]))
      } else {
        rad = Number(inputs[2]);
      }
      const acos = Math.acos(rad);
      clearData();	
      values.push(acos);
      changeButtonsValues(originFuncs);
    break;
    case "sin":
      if (deg) {
          rad = changeDegToRad(Number(inputs[2]))
      } else {
        rad = Number(inputs[2]);
      }
      const sin = Math.sin(rad);
      clearData();	
      values.push(sin);
    break;
    case "arcsin":
      if (deg) {
          rad = changeDegToRad(Number(inputs[2]))
      } else {
        rad = Number(inputs[2]);
      }
      const asin = Math.asin(rad);
      clearData();	
      values.push(asin);
      changeButtonsValues(originFuncs);
    break;
    case "tan":
      if (deg) {
          rad = changeDegToRad(Number(inputs[2]))
      } else {
        rad = Number(inputs[2]);
      }
      const tan = Math.tan(rad);
      clearData();	
      values.push(tan);
    break;
    case "arctan":
      if (deg) {
          rad = changeDegToRad(Number(inputs[2]))
      } else {
        rad = Number(inputs[2]);
      }
      const atan = Math.atan(rad);
      clearData();	
      values.push(atan);
      changeButtonsValues(originFuncs);
    break;
    case "ln": 
      const ln = Math.log(Number(inputs[2]));
      clearData();
      values.push(ln);
    break;
    case "e**": 
      const exp = Math.exp(Number(inputs[2]));
      clearData();
      values.push(exp);
      changeButtonsValues(originFuncs);
    break;
    case "log": 
      const log = Math.log10(Number(inputs[2]));
      clearData();
      values.push(log);
    break;    
    case "10**": 
      const pow10 = Math.pow(10, Number(inputs[2]));
      clearData();
      values.push(pow10);
      changeButtonsValues(originFuncs);
    break;
    case "**2": 
      const pow2 = Math.pow(Number(inputs[2]), 2);
      clearData();
      values.push(pow2);
      changeButtonsValues(originFuncs);
    break;
    case "√": 
      const sqrt = Math.sqrt(Number(inputs[2]));
      clearData();
      values.push(sqrt);
    break;
    case "√√": 
      const sqrtX = Math.pow(Number(inputs[2]), 1/Number(inputs[0]));
      clearData();
      values.push(sqrtX);
      changeButtonsValues(originFuncs);
    break;
  }   
  displayData();
  updateAnsValue(values[0]);
  return values;
}

function updateAnsValue(value) {
  calcAnsDisplay.value = `Ans = ${value}`;
}

const updateData = (value) => {
  inputs.push(value);
  inputs.shift();
  return inputs;
}

const clearValues = () => {
  values = [];
  return values
}

const clearData = () => {
  inputs = ["","",""];
	values = [];
  calcDisplay.value ="0";
  return `${inputs}, ${values}, ${calcDisplay.value}`
}

const deleteData = (values) => {
  if (values.length > 0) {
    values.pop();
  } else {
    inputs.pop();
  }
  
  return values; 
}

const displayData = () => {
  let points = 0;
  values.forEach(item => {
    if (item == '.') {
      points +=1;
    }    
  });
  if (points > 1) return calcDisplay.value = NaN;
  calcDisplay.value= inputs.join(" ") + " " + values.join("");
  return inputs.join(" ") + " " + values.join("");
}

const displayAddData = () => {
  let points = 0;
  values.forEach(item => {
    if (item == '.') {
      points +=1;
    }    
  });
  if (points > 1) return calcDisplay.value = NaN;
  calcDisplay.value= inputs.join(" ") + " " +additionInputs.join(" ")+ '' + values.join("");
  return inputs.join(" ") + " " + values.join("");
}





        