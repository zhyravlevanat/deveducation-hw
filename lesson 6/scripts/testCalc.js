describe('calcCompute', () => {
  
  it('should add in array of values value of btn, namely number found by elem id ', () => {
    const id = 12;

    const actual = calcCompute(id);

    const expected = 8;
    assert.equal(actual, expected);
  });

  it('should add in array of inputs value of btn, namely sign of operation found by elem id', () => {
    const id = 36;
    values = ['', '5'];

    const actual = calcCompute(id);

    const expected = ['', '5', '+'];
    assert.deepEqual(actual, expected);
  });

  it('should add all numbers and operation to array of inputs', () => {    
    const id = 35;
    inputs = ['', '2', '+'];
    values = ['5'];
    
    const actual = calcCompute(id);
    
    const expected = ['2', '+', '5'];
    assert.deepEqual(actual, expected);
  });

  it('should clear all data in array of values', () => {    
    const id = 7;    
    const actual = calcCompute(id);
    
    const expected = [];

    assert.deepEqual(actual, expected);
  });
  
});


describe('calc', () => {
  
  it('23 + 5 return 28', () => {
    const inputs = [23, '+', 5];    
    const actual = calc(inputs);

    const expected = 28;

    assert.equal(actual, expected);
  });

  it('23 - 5 return 18', () => {
    const inputs = [23, '-', 5];    
    const actual = calc(inputs);

    const expected = 18;

    assert.equal(actual, expected);
  });

  it('23 * 5 return 115', () => {
    inputs = [23, '×', 5];    
    const actual = calc(inputs);

    const expected = 115;

    assert.equal(actual, expected);
  });

  it('23 / 5 return 4.6', () => {
    inputs = [23, '÷', 5]; 

    const actual = calc(inputs);

    const expected = 4.6;
    assert.equal(actual, expected);
  });

  it('0 / 5 return 0', () => {
    const inputs = [0, '÷', 5];

    const actual = calc(inputs);

    const expected = 0;
    assert.equal(actual, expected);
  });  
});

describe('displayData', () => {
  
  it('should display all data', () => {
    inputs = ['', '5', '+'];
    values = ['1'];
    const actual = displayData();

    const expected = ' 5 + 1';

    assert.equal(actual, expected);
  });
});

describe('addNumbers', () => {
  
  it('3 + 5 return 8', () => {
    const firstNumber = 3;
    const secondNumber = 5;   
    const actual = addNumbers(firstNumber, secondNumber);

    const expected = 8;

    assert.equal(actual, expected);
  });

  it('0 + 5 return 5', () => {
    const firstNumber = 0;
    const secondNumber = 5;   
    const actual = addNumbers(firstNumber, secondNumber);

    const expected = 5;

    assert.equal(actual, expected);
  });

  it('0.5 + 0.9 return 1.4', () => {
    const firstNumber = 0.5;
    const secondNumber = 0.9;   
    const actual = addNumbers(firstNumber, secondNumber);

    const expected = 1.4;

    assert.equal(actual, expected);
  });

  it('NaN + 8 return NaN', () => {
    const firstNumber = '/';
    const secondNumber = 8;   
    const actual = addNumbers(firstNumber, secondNumber);

    const expected = NaN;

    assert.deepEqual(actual, expected);
  });

});

describe('subsNumbers', () => {
  
  it('3 - 5 return -2', () => {
    const firstNumber = 3;
    const secondNumber = 5;   
    const actual = subsNumbers(firstNumber, secondNumber);

    const expected = -2;

    assert.equal(actual, expected);
  });

  it('-2 - 5 return -7', () => {
    const firstNumber = -2;
    const secondNumber = 5;   
    const actual = subsNumbers(firstNumber, secondNumber);

    const expected = -7;

    assert.equal(actual, expected);
  });

  it('-0.5 - 0.9 return -1.4', () => {
    const firstNumber = -0.5;
    const secondNumber = 0.9;   
    const actual = subsNumbers(firstNumber, secondNumber);

    const expected = -1.4;

    assert.equal(actual, expected);
  });

  it('NaN - 8 return NaN', () => {
    const firstNumber = '/';
    const secondNumber = 8;   
    const actual = subsNumbers(firstNumber, secondNumber);

    const expected = NaN;

    assert.deepEqual(actual, expected);
  });  
});

describe('multNumbers', () => {
  
  it('3 * 5 return 15', () => {
    const firstNumber = 3;
    const secondNumber = 5;   
    const actual = multNumbers(firstNumber, secondNumber);

    const expected = 15;

    assert.strictEqual(actual, expected);
  });

  it('-2 * - 5 return 10', () => {
    const firstNumber = -2;
    const secondNumber = -5;   
    const actual = multNumbers(firstNumber, secondNumber);

    const expected = 10;

    assert.strictEqual(actual, expected);
  });

  it('0 * 5 return 0', () => {
    const firstNumber = 0;
    const secondNumber = 5;   
    const actual = multNumbers(firstNumber, secondNumber);

    const expected = 0;

    assert.strictEqual(actual, expected);
  });

  it('0.7 * 0.6 return 0', () => {
    const firstNumber = 0.7;
    const secondNumber = 0.6;   
    const actual = multNumbers(firstNumber, secondNumber);

    const expected = 0.42;

    assert.strictEqual(actual, expected);
  });

  it('NaN * 8 return NaN', () => {
    const firstNumber = '/';
    const secondNumber = 8;   
    const actual = multNumbers(firstNumber, secondNumber);

    const expected = NaN;

    assert.deepEqual(actual, expected);
  });  
});

describe('divNumbers', () => {
  
  it('15 / 5 return 3', () => {
    const firstNumber = 15;
    const secondNumber = 5;   
    const actual = divNumbers(firstNumber, secondNumber);

    const expected = 3;

    assert.strictEqual(actual, expected);
  });

  it('5 / 15 return 0.3333333333333333', () => {
    const firstNumber = 5;
    const secondNumber = 15;   
    const actual = divNumbers(firstNumber, secondNumber);

    const expected = 0.3333333333333333;

    assert.strictEqual(actual, expected);
  });

  it('5 / 0 return Infinity', () => {
    const firstNumber = 5;
    const secondNumber = 0;   
    const actual = divNumbers(firstNumber, secondNumber);

    const expected = Infinity;

    assert.deepEqual(actual, expected);
  });

  it('15 / -3 return -5', () => {
    const firstNumber = 15;
    const secondNumber = -3;   
    const actual = divNumbers(firstNumber, secondNumber);

    const expected = -5;

    assert.equal(actual, expected);
  });

  it('NaN / 8 return NaN', () => {
    const firstNumber = '/';
    const secondNumber = 8;   
    const actual = divNumbers(firstNumber, secondNumber);

    const expected = NaN;

    assert.deepEqual(actual, expected);
  });  
});

describe('updateData', () => {
  
  it('should change values in array inputs ', () => {
    const value = '956'    
    const actual = updateData(value);

    const expected = ['5', '+', '956'];

    assert.deepEqual(actual, expected);
  });

  it('should change values in array inputs ', () => {
    const value = '0'    
    const actual = updateData(value);

    const expected = ["+", "956", "0"];

    assert.deepEqual(actual, expected);
  });

  it('should change values in array inputs ', () => {
    const value = '+'    
    const actual = updateData(value);

    const expected = ["956", "0", "+"];

    assert.deepEqual(actual, expected);
  });  
});

describe('clearValues', () => {
  
  it('should make array of values empty', () => {
    values = ["956", "0", "+"];    
    const actual = clearValues();

    const expected = [];

    assert.deepEqual(actual, expected);
  });
});

describe('changeButtonsValues', () => {
  
  it('should change buttons values and innerHTML', () => {
    const invertedBtns = document.querySelectorAll('.calculator__button_inverted');
    const obj = {
      'cos': 'cos'      
    };    
    invertedBtns[0].value = 'tan';
    
    const actual = changeButtonsValues(obj)[0].value;
    
    const expected = 'cos';
    assert.deepEqual(actual, expected);
  });
});

describe('setAnsValue', () => {
  
  it('should set ans value in calcDisplay', () => {
    calcDisplay.value = '25';
    calcAnsDisplay.value = `Ans = 45`;
    
    const actual = setAnsValue();
    
    const expected = '45';
    assert.deepEqual(actual, expected);
  });
});

describe('setRndValue', () => {
  
  it('should set ans random value in calcDisplay', () => {
    calcDisplay.value = '25';
        
    const actual = +setRndValue();
    
    const expected = 1;
    assert.isAtMost(actual, expected);
  });
});

describe('calcPercentage', () => {
  
  it('should return array of values with caclucalted number', () => {
    inputs = ['203', '×', '15']
        
    const actual = calcPercentage(inputs);
    
    const expected = [30.45];
    assert.deepEqual(actual, expected);
  });
});

describe('clearData', () => {
  
  it('should make arrays of inputs, values, and output field empty', () => {
    inputs = ["956", "+", "0"];
    values = [];
    calcDisplay.value = '0';
    calcAnsDisplay.value = `Ans = 0`;
    const actual = clearData();

    const expected = ',,, , 0';

    assert.deepEqual(actual, expected);
  });  
});

describe('clearData', () => {
  
  it('should make arrays of inputs, values, and output field empty', () => {
    inputs = ["956", "+", "0"];
    values = [];
    calcDisplay.value = '0';

    const actual = clearData();

    const expected = ',,, , 0';
    assert.deepEqual(actual, expected);
  });  
});






