'use strict';

const buttons = ['0','1', '2', '3', '4', '5', '6','7', '8', '9', '.',
                 '+', '-', '*', '/', '=', '←', 'c'];

const calcBtnsContainer = document.getElementById('calcBtnsContainer');
const calcDisplay = document.getElementById('display');
const bntItemTemplate = document.getElementById('bntItemTemplate').innerHTML;
let inputs = ["","",""];
let values = [];

init();

function init() {
  createCalcButtons();
  calcDisplay.value = '0';
}

calcBtnsContainer.addEventListener('click', onCalcBntContainerClick);

function onCalcBntContainerClick(e) {
  calcCompute(e.target.dataset.id);   
}

function getElementById(id) {
  return document.querySelectorAll(`[data-id = "${id}"]`);
}

function calcCompute(id)  {
  const elem = getElementById(id);
  const value = elem[0].value;
  let result;
  switch(true) {
    case (id >= 0 && id <= 11):
      values.push(value);
      result = values;
      displayData();
    break;
    case (id >= 12 && id < 16):
      updateData(values.join(""));
      updateData(value);
      result = inputs;
      clearValues();
      displayData();
    break;
    case (id == 16): 
      updateData(values.join(""));
      clearValues();
      result = inputs;
      calc(inputs);
      
    break;
    case (id == 17): 
      result = deleteData(values);
      displayData();
    break;
    case (id == 18): 
      clearData();
      result = values;
    break;
  }
  
  return result;
}

function createCalcButtons() {
  buttons.forEach((elem, index) => {
    const buttonElement = bntItemTemplate.replace('{{value}}', elem)
                                          .replace('{{sign}}', elem)
                                          .replace('{{id}}', index+1);                                  
    calcBtnsContainer.innerHTML += buttonElement;
  });
}

const addNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum + secondNum;
  }
}
const multNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum * secondNum;
  }
};
const subsNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum - secondNum;
  }
};
const divNumbers = (firstNum, secondNum) => {
  if (isNaN(firstNum) || isNaN(secondNum)) {
    return NaN;
  } else {
    return firstNum / secondNum;
  }
};

const calc = (inputs) => {
  switch (inputs[1]) {
    case "+": 
      const sum = addNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();	
      values.push(sum);
    break;
    case "-": 
      const diff = subsNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();	
      values.push(diff);
    break;
    case "*": 
      const mult = multNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();	
      values.push(mult);
    break;
    case "/": 
      const div = divNumbers(Number(inputs[0]), Number(inputs[2]));
      clearData();
      values.push(div);
    break;
  }  
  displayData();
  return values;
}

const updateData = (value) => {
  inputs.push(value);
  inputs.shift();
  return inputs;
}

const clearValues= () => {
  values = [];
  return values
}

const clearData = () => {
  inputs = ["","",""];
	values = [];
  calcDisplay.value ="0";
  return `${inputs}, ${values}, ${calcDisplay.value}`
}

const deleteData = (values) => {
  if (values.length > 0) {
    values.pop();
  }
  return values; 
}

const displayData = () => {
  let points = 0;
  values.forEach(item => {
    if (item == '.') {
      points +=1;
    }    
  });
  if (points > 1) return calcDisplay.value = NaN;
  calcDisplay.value= inputs.join(" ") + " " + values.join("");
  return inputs.join(" ") + " " + values.join("");
}





        