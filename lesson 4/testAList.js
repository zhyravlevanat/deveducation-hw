describe('push', () => {
  
  it('if 1 args, should add this to end of the array return increased length of array ', () => {

    const list = new AList();
    list.store = [1];
    const elem = 2;

    const actual = list.push(elem);

    const expected = 2;
    assert.equal(actual, expected);
  });

  it('if more than 1 args return increased by the number of args length of array ', () => {

    const list = new AList();
    list.store = [1, 2];
    const elem1 = 2;
    const elem2 = 4;

    const actual = list.push(elem1, elem2);

    const expected = 4;
    assert.equal(actual, expected);
  });

  it('if no args return current length of array ', () => {

    const list = new AList();
    list.store = [1];

    const actual = list.push();

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('pop', () => {
  
  it('should delete 1 element from the end of array return deleted element', () => {

    const list = new AList();
    list.store = [1, 3, 5];

    const actual = list.pop();

    const expected = 5;
    assert.equal(actual, expected);
  });

  it('if no elements in array return undefined ', () => {

    const list = new AList();
    list.store = [];

    const actual = list.pop();

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('if add some argument return deleted element ', () => {

    const list = new AList();
    list.store = [1, 8];

    const actual = list.pop(2);

    const expected = 8;
    assert.equal(actual, expected);
  });
});

describe('shift', () => {
  
  it('should delete 1 element from the beginning of array return deleted element', () => {

    const list = new AList();
    list.store = [1, 3, 5];

    const actual = list.shift();

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('if no elements in array return undefined ', () => {

    const list = new AList();
    list.store = [];

    const actual = list.shift();

    const expected = undefined;
    assert.equal(actual, expected);
  });

  it('if add some argument all the same return deleted element ', () => {

    const list = new AList();
    list.store = [1, 8];

    const actual = list.shift(2);

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('unshift', () => {
  
  it('if 1 args, should add this to the beginning of the array return increased length of array ', () => {

    const list = new AList();
    list.store = [1];
    const elem = 2;

    const actual = list.unshift(elem);

    const expected = 2;
    assert.equal(actual, expected);
  });

  it('if more than 1 args return increased by the number of args length of array ', () => {

    const list = new AList();
    list.store = [1, 2];
    const elem1 = 2;
    const elem2 = 4;

    const actual = list.unshift(elem1, elem2);

    const expected = 4;
    assert.equal(actual, expected);
  });

  it('if no args return current length of array ', () => {

    const list = new AList();
    list.store = [1];

    const actual = list.unshift();

    const expected = 1;
    assert.equal(actual, expected);
  });
});

describe('toString', () => {
  
  it('should return string with all elements of array separated by commas', () => {

    const list = new AList();
    list.store = [1, 2, 3];

    const actual = list.toString();

    const expected = '1, 2, 3';
    assert.equal(actual, expected);
  });

  it('if there are no elements in array should return empty string ', () => {

    const list = new AList();
    list.store = [];

    const actual = list.toString();

    const expected = '';
    assert.equal(actual, expected);
  });  
});

describe('sort', () => {
  
  it('should return array with elements sorted by ascending', () => {

    const list = new AList();
    list.store = [1, 7, 3, 9];

    const actual = list.sort();

    const expected = [1, 3, 7, 9];
    assert.deepEqual(actual, expected);
  });

  it('if array length 1 return current array ', () => {

    const list = new AList();
    list.store = [1];

    const actual = list.sort();

    const expected = [1];
    assert.deepEqual(actual, expected);
  });

  it('if array is empty return empty array', () => {

    const list = new AList();
    list.store = [];

    const actual = list.sort();

    const expected = [];
    assert.deepEqual(actual, expected);
  });
});