class AList {
  store = [];

  push(...rest) {
    const length = this.store.length;
    this.store.length = this.store.length + rest.length
        
    for (let i = length; i < this.store.length; i++) {
      this.store[i] = rest[i-length];
    }

    return this.store.length;
  }

  pop() {
    let deletedElem;

    if (this.store.length > 0) {
      deletedElem = this.store[this.store.length-1];
      this.store.length--;
    } else {
      return undefined
    }

    return deletedElem;
  }

  shift() {
    let deletedElem;

    if (this.store.length > 0) {
      deletedElem = this.store[0];
      for (let i = 0; i< this.store.length; i++) {
        this.store[i] = this.store[i+1];
      }
      this.store.length--;
    } else {
      return undefined
    }

    return deletedElem;
  }

  unshift(...rest) {
    this.store.length = this.store.length + rest.length;
    
    for (let i = rest.length; i < this.store.length; i++) {
      this.store[i] = this.store[i - rest.length];
    }

    for (let i = 0; i< rest.length; i++) {
      this.store[i] = rest[i];
    }

    return this.store.length;    
  }

  toString() {
    let result = '';
    for (let i = 0; i < this.store.length; i++) {
      if (i != this.store.length -1) {
        result += `${this.store[i]}, `
       
        
      } else  result += `${this.store[i]}`;
     
    }

    return result;
  } 

  localSort(first, second) {
    if (String(first) > String(second)) {
        return 1;
    } else if (String(first) < String(second)){
        return -1;
    }

    return 0;
}

  sort(callback) {
    const comparator = callback || this.localSort; 
    if (!Array.isArray(this.store)) return "not valid data";
    if (this.store.length === 0) return [];
    if (this.store.length === 1) return this.store;

    for(let i = 0; i < this.store.length; i++) {
      if (isNaN(this.store[i])) {
        return "not valid data";
      } 
    }

    for (let i = 0; i < this.store.length-1; i++) {
      for (let j = 0; j < this.store.length-1-i; j++) {
        const compareResults = comparator(this.store[j], this.store[j+1]);
        if (compareResults > 0) {
          const temp = this.store[j+1];
          this.store[j+1] = this.store[j];
          this.store[j] = temp;
        }
      }
    }
    return this.store;
  }
}
const list = new AList();

console.log(list.push(3, 2, 5, 11), list.store);
console.log(list.pop(), list.store);
console.log(list.shift(), list.store);
console.log(list.unshift(9, 5, 4), list.store);
console.log(list.toString(), list.store);
console.log(list.sort());