describe('push', () => {
  
  it('should insert node at the end of the list, if root is null, elem will be assigned to the root, return increased length of list ', () => {

    const list = new LList();
    list.root = null;
    const elem = 3;

    const actual = list.push(elem);

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('should insert node at the end of the list return increased length of list ', () => {

    const list = new LList();
    list.root = {value: 3, next: {value: 4, next: null}};
    const elem = 2;

    const actual = list.push(elem);

    const expected = 3;
    assert.equal(actual, expected);
  });
});

describe('pop', () => {
  
  it('should delete 1 element from the end of list return deleted element', () => {

    const list = new LList();
    list.root = {value: 3, next: {value: 2, next: null}};

    const actual = list.pop();

    const expected = {value: 2, next: null};
    assert.deepEqual(actual, expected);
  });

  it('if no elements in list return undefined ', () => {

    const list = new LList();
    list.root = null;

    const actual = list.pop();

    const expected = undefined;
    assert.equal(actual, expected);
  });  
});

describe('shift', () => {
  
  it('should delete 1 element from the beginning of list return deleted element', () => {

    const list = new LList();
    list.root = {value: 3, next: {value: 2, next: null}};

    const actual = list.shift();

    const expected = {value: 3, next: null};
    assert.deepEqual(actual, expected);
  });

  it('if no elements in list return undefined ', () => {

    const list = new LList();
    list.root = null;

    const actual = list.shift();

    const expected = undefined;
    assert.equal(actual, expected);
  });  
});

describe('unshift', () => {
  
  it('should insert node at the beginning of the list, if root is null, elem will be assigned to the root, return increased length of list  ', () => {

    const list = new LList();
    list.root = null;
    const elem = 3;

    const actual = list.push(elem);

    const expected = 1;
    assert.equal(actual, expected);
  });

  it('should insert node at the beginning of the list, return increased length of list  ', () => {

    const list = new LList();
    list.root = {value: 3, next: null};
    const elem = 3;

    const actual = list.push(elem);

    const expected = 2;
    assert.equal(actual, expected);
  }); 
});

describe('toString', () => {
  
  it('should return string with all values of list separated by commas', () => {

    const list = new LList();
    list.root = {value: 3, next: {value: 2, next: {value: 5, next: null}}};

    const actual = list.toString();

    const expected = '3, 2, 5';
    assert.equal(actual, expected);
  });

  it('if there are no elements in list should return empty string ', () => {

    const list = new LList();
    list.root = null;

    const actual = list.toString();

    const expected = '';
    assert.equal(actual, expected);
  });  
});

