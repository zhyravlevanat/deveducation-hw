class LList {
  root = null;
   
  createNode(value, next = null) {
    return {
      value,
      next
    }
  }

  getNumberOfElems(list) {
    let counter = 0;
    while(list.next !== null){
      counter++;
      list = list.next;
    }
    return ++counter;
  }

  push(...rest) {
    if (!this.root) {
      this.root = this.createNode(rest[rest.length-1]);
      if (rest.length > 1) {
        for (let i = 1; i < rest.length; i++) {  
          this.root = this.createNode(rest[rest.length-i-1], this.root);         
        }
      }      
    } else {
      for (let i = 0; i < rest.length; i++) {          
        let node = this.root;
        while (node.next != null) {
          node = node.next;               
        }
        node.next = this.createNode(rest[i]);        
      }       
    }
    const counter = this.getNumberOfElems(this.root);
    return counter;    
  }

  pop() {
    let deletedNode;

    if(!this.root){
      return undefined;
    }
  
    if(!this.root.next){
        this.root = null;
        return;
    }

    let prevNode = this.root;
    let nextNode = this.root.next;
  
    while(nextNode.next !== null){
        prevNode = nextNode;
        nextNode = nextNode.next;
    }
      
    deletedNode = nextNode;
    prevNode.next = null;

    return deletedNode;
  }

  shift() {
    let deletedNodeValue;

    if (!this.root){
      return undefined;
    }
  
    if (!this.root.next){
        this.root = null;
        return;
    }

    deletedNodeValue = this.root.value;
    this.root = this.root.next;   
    
    return {value: deletedNodeValue, next: null}
  }

  unshift(...rest) {
    if (!this.root) {
      this.root = this.createNode(rest[rest.length-1]);
      if (rest.length > 1) {
        for (let i = 1; i < rest.length; i++) {          
          this.root = this.createNode(rest[rest.length-i-1], this.root);
        }
      }      
    } else {
      for (let i = 0; i < rest.length; i++) {          
        this.root = this.createNode(rest[rest.length-i-1], this.root);
      }        
    }
    const counter = this.getNumberOfElems(this.root);
    return counter;
  }

  toString() {
    let nextNode = this.root;
    if (nextNode == null) {
      return '';
    }
    let result = `${nextNode.value}, `;
    while(nextNode.next !== null){
      nextNode = nextNode.next;
      result += `${nextNode.value}, `;
    }   
    return result.slice(0, result.length-2)   
  }

}
const linkedList = new LList();

console.log(linkedList.push(3, 5, 6), linkedList.root);
console.log(linkedList.push(2, 4), linkedList.root);
// console.log(linkedList.unshift(3, 4, 8), linkedList.root);
// console.log(linkedList.unshift(0), linkedList.root);
// console.log(linkedList.pop(), linkedList.root);
// console.log(linkedList.shift(), linkedList.root);
console.log(linkedList.toString(), linkedList.root);
